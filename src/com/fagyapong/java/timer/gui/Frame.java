package com.fagyapong.java.timer.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Frame extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private static JLabel hrLabel = new JLabel("Hour", SwingConstants.CENTER);
	private static JLabel minLabel = new JLabel("Minutes", SwingConstants.CENTER);
	private static JLabel secLabel = new JLabel("Seconds", SwingConstants.CENTER);
	
	private static JLabel timeLabel = new JLabel("0:0:0", SwingConstants.CENTER);
	
	private static JTextField hrTxtField = new JTextField(3);
	private static JTextField minTxtField = new JTextField(4);
	private static JTextField secTxtField = new JTextField(4);

	private static JButton startButton = new JButton("Start");
	private static JButton stopButton = new JButton("Stop");
	private static JButton resetButton = new JButton("Reset");
	private static JButton clrButton = new JButton("Clear");
	
	private static JPanel[] framePanels = new JPanel[6];
	
	private static Timer timer;
	private static int timeEnd;
	private static int timeRemaining;
	
	private static int hourRem;
	private static int minRem;
	private static int secRem;
	
	private static JProgressBar progBar = new JProgressBar(0, 100);
	
	public Frame() {
		
		setTitle("Timer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		timer = new Timer(1000, new TimerActionListener());
		setSize(350, 170);
		setLayout(new GridLayout(6, 1));
		initPanels();
		for (JPanel jp : framePanels) {
			add(jp);
		}

		startButton.addActionListener(this);
		stopButton.addActionListener(this);
		resetButton.addActionListener(this);
		clrButton.addActionListener(this);
		
		progBar.setValue(0);
		progBar.setStringPainted(true);
		setLook();
		
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand().toString();
		switch (command) {
			case "Start":
				timeEnd = getTime();
				timeRemaining = timeEnd;
				timer.start();
				startButton.setEnabled(false);
				break;
				
			case "Stop":
				timer.stop();
				startButton.setEnabled(true);
				break;
				
			case "Reset":
				timer.stop();
				timeRemaining = timeEnd;
				timer.start();
				break;
				
			case "Clear":
				timer.stop();
				clearFields();
				break;
		}
	}
	public void setLook() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.updateComponentTreeUI(this);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void clearFields() {
		hrTxtField.setText("");
		minTxtField.setText("");
		secTxtField.setText("");
		progBar.setValue(0);
		hourRem = minRem = secRem = 0;
		timeLabel.setText(hourRem + ":" + minRem + ":" + secRem);
	}
	
	public static int getTime() {
		int result;
		int hour = (Integer.parseInt(hrTxtField.getText())) * 3600;
		int minutes = (Integer.parseInt(minTxtField.getText())) * 60;
		int seconds = (Integer.parseInt(secTxtField.getText()));
		
		result = hour + minutes + seconds;
		return result;
	}
	
	public static void initPanels() {
		for (int i = 0; i < framePanels.length; i++) {
			framePanels[i] = new JPanel();
		}
		
		framePanels[0].setLayout(new GridLayout(1, 2));
		framePanels[1].setLayout(new GridLayout(1, 2));
		framePanels[2].setLayout(new GridLayout(1, 2));
		framePanels[3].setLayout(new GridLayout(1, 1));
		framePanels[4].setLayout(new GridLayout(1, 1));
		framePanels[5].setLayout(new GridLayout(1, 4));
		
		framePanels[0].add(hrLabel);
		framePanels[0].add(hrTxtField);
		
		framePanels[1].add(minLabel);
		framePanels[1].add(minTxtField);
		
		framePanels[2].add(secLabel);
		framePanels[2].add(secTxtField);
		
		framePanels[3].add(progBar);
		progBar.setVisible(true);
		framePanels[4].add(timeLabel);
		
		framePanels[5].add(startButton);
		framePanels[5].add(stopButton);
		framePanels[5].add(resetButton);
		framePanels[5].add(clrButton);
	}
	
	class TimerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			
			if (timeRemaining == 0) {
				timer.stop();
			}
			else {
				timeRemaining -= 1;
				hourRem = (timeRemaining / 3600);
				minRem = ((timeRemaining % 3600) / 60);
				secRem = (timeRemaining % 60);
				progBar.setValue(100 - ((int)(((double)timeRemaining / timeEnd) * 100)));
				timeLabel.setText(hourRem + ":" + minRem + ":" + secRem);
			}
		}
	}
}